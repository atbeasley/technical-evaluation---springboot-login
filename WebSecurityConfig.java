package login;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.anyRequest().fullyAuthenticated()
				.and()
			.formLogin();
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
		.ldapAuthentication()
        .userSearchFilter("(uid={0})")
        .userSearchBase("dc=example,dc=com")
        .groupSearchFilter("uniqueMember={0}")
        .groupSearchBase("ou=mathematicians,dc=example,dc=com")
        .userDnPatterns("uid={0}")
        .contextSource()
        .url("ldap://ldap.forumsys.com:389")
        .managerDn("cn=read-only-admin,dc=example,dc=com")
        .managerPassword("password");
        
	}

}
